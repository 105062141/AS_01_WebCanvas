var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
ctx.lineCap = "round";
ctx.fillStyle = "white"; //整個canvas塗上白色背景避免PNG的透明底色效果
ctx.fillRect(0, 0, 760, 780);

var canvaspos = getPosition(canvas);
var brueraMode = false;
var drawMode = false;
var eraserMode = false;
var textMode = false;
var circleMode = false;
var recMode = false;
var triMode = false;
var mouseposX = 0;
var mouseposY = 0;

//position
function getPosition(element){
    var x = 0;
    var y = 0;
    while(element){
        x += element.offsetLeft - element.scrollLeft + element.clientLeft;
        y += element.offsetTop - element.scrollTop + element.clientTop;
        element = element.offsetParent;
    }
    return{x : x, y : y};
}

//color
var color = document.getElementById("myColor");

//linewidth
var width = document.getElementById("myRange");
var widthValue = document.getElementById("rangeValue");
//width.step = "2";
widthValue.innerHTML = width.value;
width.oninput = function() {
    widthValue.innerHTML = this.value;
}

//text
var text = document.getElementById("myText");
var area = document.getElementById("myTextArea");
var enter = document.getElementById("myEnter");
var selectSize = document.getElementById("mySelectSize");
var selectFamily = document.getElementById("mySelectFamily");
text.addEventListener('click', function(e){
    textMode = true;
    brueraMode = false;
    eraserMode = false;
    circleMode = false;
    recMode = false;
    triMode = false;
    canvas.style.cursor = "text";
});
enter.addEventListener('click', function(elem){
    area.style.display = "none";
    ctx.strokeStyle = color.value;
    ctx.font = selectSize.value + " " + selectFamily.value;
    ctx.strokeText(area.value, mouseposX - 35, mouseposY - 20);
    cPush();
});

//brush
var brush = document.getElementById("myBrush");
brush.addEventListener('click', function(e){
    brueraMode = true;
    eraserMode = false;
    textMode = false;
    circleMode = false;
    recMode = false;
    triMode = false;
    canvas.style.cursor = "url(images/brush.png), auto";
});

//eraser
var eraser = document.getElementById("myEraser");
eraser.addEventListener('click', function(e){
    brueraMode = true;
    eraserMode = true;
    textMode = false;
    circleMode = false;
    recMode = false;
    triMode = false;
    canvas.style.cursor = "url(images/eraser.png), auto";
});

canvas.addEventListener('mousedown', mousedownfunction);
canvas.addEventListener('mousemove', mousemovefunction);
canvas.addEventListener('mouseup', function(e){
    drawMode = false;
    cPush();
});

function mousedownfunction(e){
    if(textMode){
        mouseposX = e.pageX - canvaspos.x + 25;
        mouseposY = e.pageY - canvaspos.y + 25;
        area.style.display = "block";
        area.value = "";
        area.style.left = mouseposX + 'px';
        area.style.top = mouseposY + 'px';
        ctx.lineWidth = 1;
    }
    else if(circleMode){
        ctx.beginPath();
        ctx.lineWidth = "4";
        ctx.strokeStyle = "#000000";
        ctx.arc(e.pageX - canvaspos.x, e.pageY - canvaspos.y, 100, 0, 2 * Math.PI, false);
        ctx.stroke();
        cPush();
    }
    else if(recMode){
        ctx.beginPath();
        ctx.lineWidth = "4";
        ctx.strokeStyle = "#000000";
        ctx.rect(e.pageX - canvaspos.x, e.pageY - canvaspos.y, 290, 140); 
        ctx.stroke();
        cPush();
    }
    else if(triMode){
        ctx.beginPath();
        ctx.lineWidth = "4";
        ctx.strokeStyle = "#000000";
        ctx.moveTo(e.pageX - canvaspos.x, e.pageY - canvaspos.y);
        ctx.lineTo(e.pageX - canvaspos.x - 80, e.pageY - canvaspos.y + 200);
        ctx.lineTo(e.pageX - canvaspos.x + 80, e.pageY - canvaspos.y + 200);
        ctx.closePath();
        ctx.stroke();
        cPush();
    }
    else if(brueraMode){
        ctx.beginPath();
        ctx.moveTo(e.pageX - canvaspos.x + 15, e.pageY - canvaspos.y + 55);
        ctx.lineWidth = 2 * width.value;
        drawMode = true;
        if(eraserMode){
            ctx.strokeStyle = "#FFFFFF";
        }
        else{
            ctx.strokeStyle = color.value;
        }
    }
    else{
    }
}

function mousemovefunction(e){
    if (drawMode) {
        ctx.lineTo(e.pageX - canvaspos.x + 10, e.pageY - canvaspos.y + 55);
        ctx.stroke();
    }
}

//circle
var circle = document.getElementById("myCircle");
circle.addEventListener('click', function(e){
    circleMode = true;
    brueraMode = false;
    eraserMode = false;
    textMode = false;
    recMode = false;
    triMode = false;
    canvas.style.cursor = "crosshair";
});

//rectangle
var rectangle = document.getElementById("myRectangle");
rectangle.addEventListener('click', function(e){
    recMode = true;
    brueraMode = false;
    eraserMode = false;
    textMode = false;
    circleMode = false;
    triMode = false;
    canvas.style.cursor = "crosshair";
});

//triangle
var triangle = document.getElementById("myTriangle");
triangle.addEventListener('click', function(e){
    triMode = true;
    brueraMode = true;
    eraserMode = true;
    textMode = false;
    circleMode = false;
    recMode = false;
    canvas.style.cursor = "crosshair";
});

//image
var image = document.getElementById("myImage");
image.addEventListener('change', function(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img, 350, 300);
        }    
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]); 
});

//undo
var undo = document.getElementById("myUndo");
undo.addEventListener('click', function(e){
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function(){
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
    else{
        cStep--;
        ctx.fillStyle = "white"; //整個canvas塗上白色背景避免PNG的透明底色效果
        ctx.fillRect(0, 0, 760, 780);
    }
    area.style.display = "none";
});

//redo
var redo = document.getElementById("myRedo");
redo.addEventListener('click', function(e){
    if (cStep < cPushArray.length - 1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function(){
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
});

var cPushArray = new Array();
var cStep = -1;
function cPush() {
    cStep++;
    if (cStep < cPushArray.length)
        cPushArray.length = cStep;
    cPushArray.push(canvas.toDataURL());
}

//reset
var reset = document.getElementById("myReset");
reset.addEventListener('click', function(e){
    ctx.fillStyle = "white"; //整個canvas塗上白色背景避免PNG的透明底色效果
    ctx.fillRect(0, 0, 760, 780);
    area.style.display = "none";
    cPushArray.length = 0;
    cStep = -1;
});

//save as URL
var save = document.getElementById("mySave");
save.addEventListener('click', function(e){
    var saveImage = canvas.toDataURL("image/jpg").replace("image/jpg", "image/octet-stream");
    save.setAttribute("href", saveImage);
});