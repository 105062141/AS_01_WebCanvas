# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                 **Item**                 | **Score** |
| :--------------------------------------: | :-------: |
|             Basic components             |    60%    |
|              Advance tools               |    35%    |
|         Appearance (subjective)          |    5%     |
| Other useful widgets (**describe on README.md**) |   1~10%   |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


## Functions

* getPosition(): 取得某個element的位置。用來取得 canvas 的位置來避免筆刷與游標之間的位移差。
* mousedownfunction(e): 當偵測到 canvas 上 mousedown 的時候會被呼叫。並依照現在屬於哪一個模式去執行該模式的繪畫功能。
*  mousemovefunction(e): 當偵測到 canvas 上 mousemove 的時候會被呼叫。只有 mode 為 brush 或 eraser 、且 drawMode == true 時才需要用到。會沿著滑鼠的軌跡去畫出線條，並且 ctx.stroke() 畫上畫布。
* cPush(): 會將當下的每個畫布狀態存成 URL （呼叫 .toDataURL()），然後存進 cPushArray 這個 array 裡面，以利 undo、redo 功能的執行。只要畫布上有任何的改變都要呼叫這個 function 來存取。 
* Brush and eraser:
  * brush.addEventListener('click', function(e){ }) : 當 brush 這個 button 被 click 之後，即進入筆刷模式，並把游標變成筆刷的圖案；在這裡要把其他 mode 都關掉。
  * eraser.addEventListener('click', function(e){ }): 當 eraser 這個button 被 click 之後，即進入橡皮擦模式，並把游標變成橡皮擦的圖案；在這裡要把其他 mode 都關掉。
  * canvas.addEventListener('mousedown', mousedownfunction): 當  mousedown 會進入 mousedownfunction ，並進到屬於他們的 mode 裡面，定位游標的位置、設定筆刷（橡皮擦）的粗細和顏色，並把 drawMode = true 確保會被 mousemovefunction 偵測到。
  * canvas.addEventListener('mousemove', mousemovefunction)
  * canvas.addEventListener('mouseup', function(e){ }): 退出 drawMode 模式，cPush() 存取當前的筆畫。
* Text input:
  * text.addEventListener('click', function(e){ }): 進入 textMode 模式，設定文字游標。
  * canvas.addEventListener('mousedown', mousedownfunction):  在 textMode 裡面，讓原本預設好的 input area display 出來，成為使用者的文字輸入區。
  * enter.addEventListener('click', function(elem){ }): 之後按下 Enter 這個 button 時，就將文字根據設定的大小和字體印在畫布上，並將 input area display none 隱藏起來，再 cPush() 當前的狀態。 
* Cursor icon:
  * canvas.style.cursor = " " : 分別在不同的功能底下去設定他所屬的游標。
* Refresh button:
  * reset.addEventListener('click', function(e){ }): 當按下 Reset button，就將整個畫面重新刷白，並且把 cPushArray 裡的東西都清空，以達到重新開始的效果。
* Different brush shapes:
  * circle.addEventListener('click', function(e){ }): 圓圈模式、游標，mousedown 之後設定線條粗細和顏色（黑），並畫一個圓之後 cPush()。
  * rectangle.addEventListener('click', function(e){ }): 長方形模式、游標，mousedown 之後設定線條粗細和顏色（黑），並畫一個長方形之後 cPush()。
  * triangle.addEventListener('click', function(e){ }): 三角形模式、游標，mousedown 之後設定線條粗細和顏色（黑），並畫一個三角形之後 cPush()。
* Un/Re-do button:
  * undo.addEventListener('click', function(e){ }): 取出上一個狀態存在 cPushArray 裡面的 URL，並且畫在畫布上。
  * redo.addEventListener('click', function(e){ }): 取出下一個狀態存在 cPushArray 裡面的 URL，並且畫在畫布上。
* Image tool:
  * image.addEventListener('change', function(e){ }): 用 input 中 type = "file" 搭配 FileReader()，去選取電腦中的圖片檔案，上傳到畫布上，當圖片 load 完就 drawImage() 畫到畫布上。
* Download:
  * save.addEventListener('click', function(e){ }): 用 a 中 download = "(檔名)" 把所畫的檔案存取下來。